package com.murphy.gatewaydemo.controller;

import com.murphy.gatewaydemo.response.BaseResponse;
import com.murphy.gatewaydemo.response.OrderResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author dongsufeng
 * @date 2019/11/19 16:41
 * @version 1.0
 */
@RestController
@Log4j2
public class UserController {
	@Autowired
	RestTemplate restTemplate;

	@GetMapping("user")
	public BaseResponse get(String userId){
		log.info("userController========================={}=userId={}",this.getClass().getSimpleName(),userId);
//		try {
//			Thread.sleep(100);
//			ResponseEntity<String> forEntity = restTemplate
//					.getForEntity("http://localhost:8002/message-info/message", String.class);
//			return this.getClass().getSimpleName()+"====="+forEntity.getBody();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		OrderResponse orderResponse = new OrderResponse();
		orderResponse.setOrderNo("sdfnaw;efn;o1i2312312312");
		orderResponse.setDesc("get 第一个");
		BaseResponse baseResponse = new BaseResponse("1000","成功",orderResponse);
		return baseResponse;
	}
	@PostMapping("post_user")
	public BaseResponse post(@RequestBody String userId){
		log.info("{}===={}",this.getClass().getSimpleName(),userId);
		BaseResponse baseResponse = new BaseResponse("1000","成功","UgoKepLLGgRlkis8wx2IOW6ak6X/lGwGORDeg0Gy0RTdph5Vf0J5zajyUkotkkxQTGtuwqrNNQ7w4nN346+yug==");
		baseResponse.setRandomKey("M7EbOfC1T6yN8//IE1w1uLRZwpSpg0LQm/BI4ZngDGj33F0ECPwhi2IkSa1C2mQQtAIk4UzR97rTytAl2YYYLTIdPT8xyw2Rl4MAjDKekMjWheF+sKL0tw1uZw3t2ToxmZNWw7/8T9EKHas8fA+5ZL4FS4ma+y17XlRy1musQ+A=");
		baseResponse.setSign("nCRP3dd9Nt+PEjmEW5Xj6xztDA54WTzbT9jTej/0ml8pFOH+IaqtofDwOuCgt2cjysjpG4GrCYUWvj+D7tuhRgLEIgT+dYvUrsZGh+Hu2mq+dWka7Ker7Fauun4NV4pr9WxbKV0UgUWunKrF9V9icnPJR77A3pr8+We31oDpOts=");
		return baseResponse;
	}
}
