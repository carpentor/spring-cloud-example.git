package com.murphy.security.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.http.HttpServletRequest;

/**
 * 非用户名密码验证
 * @author dongsufeng
 * @version 4.0
 * @date 2019/12/3 3:48 PM
 */
public interface UserExtendService extends UserDetailsService{
    /**
     * 业务校验
     * @param request
     * @return
     */
    default boolean validate(HttpServletRequest request){
        return true;
    };

	/**
	 * 认证失败扩展操作
	 * @param request
	 */
	default void authenticatingFailureHandler(HttpServletRequest request){};

	/**
	 * 认证成功扩展操作
	 * @param request
	 */
	default void authenticatingSuccessHandler(HttpServletRequest request){};
}
