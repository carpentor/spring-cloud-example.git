package com.murphy.security.handler;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.murphy.security.cache.Cache;
import com.murphy.security.service.UserExtendService;
import com.murphy.security.token.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 登陆成功后处理
 * @author dongsufeng
 * @date 2019/12/02 15:01
 * @version 1.0
 */
@Component
public class SimpleAuthenticatingSuccessHandler implements AuthenticationSuccessHandler {

    private static ObjectMapper globalObjectMapper = new ObjectMapper();

    @Autowired
    private TokenService tokenService;
	@Autowired
	private UserExtendService userExtendService;
	@Autowired
	Cache cacheManager;

    static {
        globalObjectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    private Logger logger = LoggerFactory.getLogger(SimpleAuthenticatingSuccessHandler.class);

    private static void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

	/**
	 * 登陆成功返回token给用户
	 * @param authentication
	 * @throws IOException
	 * @throws ServletException
	 */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		logger.info("登录成功返回对象==={}",JSONObject.toJSONString(authentication.getPrincipal()));
		Map<String, String> result = new HashMap<>();
		result.put("token", tokenService.generateToken(authentication));
        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		Object o = cacheManager.get(authentication.getName());
		if (o != null){
			cacheManager.remove(o.toString());
		}
        cacheManager.put(result.get("token"),authentication.getName(),7, TimeUnit.DAYS);
        cacheManager.put(authentication.getName(),result.get("token"),7, TimeUnit.DAYS);
        globalObjectMapper.writeValue(response.getWriter(), result);
        clearAuthenticationAttributes(request);
		try {
			userExtendService.authenticatingSuccessHandler(request);
		} catch (Exception e) {
		}
	}

}

