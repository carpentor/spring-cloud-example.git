package com.murphy.security.cache;

import com.murphy.security.exception.CacheException;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author dongsufeng
 * @date 2021/9/8 20:18
 * @version 1.0
 */
public class RedisCache<K,V> implements Cache<K,V>{
	private RedisTemplate<K,V> redisTemplate;
	public RedisCache(RedisTemplate<K,V> redisTemplate){
		this.redisTemplate=redisTemplate;
	}
	@Override
	public V get(K var1) throws CacheException {
		return redisTemplate.opsForValue().get(var1);
	}

	@Override
	public V put(K var1, V var2,long timeout, TimeUnit timeUnit) throws CacheException {
		 redisTemplate.opsForValue().set(var1,var2);
		 redisTemplate.expire(var1,timeout,timeUnit);
		 return var2;
	}

	@Override
	public V remove(K var1) throws CacheException {
		V v = redisTemplate.opsForValue().get(var1);
		redisTemplate.delete(var1);
		return v;
	}

	@Override
	public void clear() throws CacheException {}

	@Override
	public boolean hasKey(K var1){
		return redisTemplate.hasKey(var1);
	}
}
